package com.example.data

import kotlinx.serialization.Serializable

@Serializable
data class Todo(
    var id: Int,
    var title : String,
    var done: Boolean
)