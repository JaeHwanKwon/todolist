package com.example

import io.ktor.server.engine.*
import io.ktor.server.netty.*
import com.example.plugins.*
import org.kodein.di.bind
import org.kodein.di.ktor.di
import org.kodein.di.singleton
import java.security.SecureRandom
import java.util.*

fun main() {
    embeddedServer(Netty, port = 8080, host = "localhost") {
        di {
            bind {
                singleton { environment.config }
            }
        }
        configureRouting()
        configureHTTP()
        configureSerialization()
    }.start(wait = true)
}
